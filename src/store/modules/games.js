import { fetchApi } from '@/utils'
import * as types from '../mutation-types'
import router from '@/router'

const gamesState = {
  games: [],
  playing: {
    room: {},
    players: [],
  },
  invitedFriends: [],
}

const getters = {
  games: state => state.games,
  getGameById: state => gameId => state.games.find(x => x && x.id === gameId),
  getGameBySlug: state => gameSlug =>
    state.games.find(x => x && x.slug === gameSlug),
  isGameRoomByCode: state => code =>
    state.playing.room.code === parseInt(code, 10),
  getGameRoom: state => state.playing.room,
  getPlayers: state => state.playing.players,
  getPlayerById: state => playerId =>
    state.playing.players.find(x => x.id === playerId),
  getInvitedFriends: state => state.invitedFriends,
}

const actions = {
  async fetchGames({ commit, dispatch }) {
    dispatch('startLoading', 'fetchGames')

    const { data } = await fetchApi({ input: 'games' })

    commit(types.SET_GAMES, data)

    dispatch('stopLoading', 'fetchGames')
  },

  async createGameRoom(_, gameId) {
    const { data } = await fetchApi({
      input: 'games',
      method: 'post',
      data: { gameId },
    })

    router.push({ name: 'game-lobby', params: { code: data.code } })
  },

  async fetchGameRoom({ commit, dispatch, getters }, code) {
    dispatch('startLoading', 'fetchGameRoom')

    const { data } = await fetchApi({
      input: `games/room/${code}`,
    })

    commit(types.SET_GAME_ROOM, data)
    commit(types.SET_GAME_ROOM_FRIEND_INVITES, data.friend_invites)

    if (getters.getPlayers.length === 0) {
      dispatch('startLoading', 'players')
    }

    dispatch('stopLoading', 'fetchGameRoom')
  },

  async toggleReady({ commit, getters }, { playerId, status, toFetch }) {
    if (toFetch) {
      let toChange = getters.getPlayerById(playerId).status

      if (toChange === 'ready') {
        toChange = 'not_ready'
      } else if (toChange === 'not_ready') {
        toChange = 'ready'
      }

      const { data } = await fetchApi({
        input: `games/room/${getters.getGameRoom.code}/ready/${playerId}`,
        method: 'put',
        data: {
          status: toChange,
        },
      })

      commit(types.CHANGE_STATUS, { user_id: playerId, status: toChange })
      return { data, status: toChange }
    } else {
      commit(types.CHANGE_STATUS, { user_id: playerId, status })
    }
  },
  async changeRounds({ commit, getters, dispatch }, rounds) {
    dispatch('startLoading', 'changeRounds')

    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `games/room/${code}`,
      method: 'put',
      data: {
        rounds,
      },
    })

    commit(types.SET_GAME_ROOM, data)

    dispatch('stopLoading', 'changeRounds')
  },

  async changeGrid({ commit, getters, dispatch }, grid) {
    dispatch('startLoading', 'changeGrid')

    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `games/room/${code}`,
      method: 'put',
      data: {
        grid,
      },
    })

    commit(types.SET_GAME_ROOM, data)

    dispatch('stopLoading', 'changeGrid')
  },

  async inviteFriend({ commit, dispatch, getters }, friendId) {
    if (!getters.getInvitedFriends.includes(friendId)) {
      dispatch('startLoading', `gameRoomInviteFriend${friendId}`)

      let code = getters.getGameRoom.code

      const { data } = await fetchApi({
        input: `games/room/${code}/friends/${friendId}`,
        method: 'post',
      })

      commit(types.SET_GAME_ROOM_FRIEND_INVITES, data)

      dispatch('stopLoading', `gameRoomInviteFriend${friendId}`)
    }
  },

  async respondToInvite({ dispatch }, { status, code, userId }) {
    const { data } = await fetchApi({
      input: `games/room/${code}/friends/${userId}`,
      method: 'put',
      data: { status },
    })

    if (status === 'accept') {
      dispatch('startLoading', 'players')

      router.push({ name: 'game-lobby', params: { code: data.code } })
    }
  },

  addPlayer({ commit, getters }, player) {
    if (!getters.getPlayerById(player.user_id)) {
      commit(types.ADD_GAME_ROOM_PLAYER, player)
    }
  },

  removePlayer({ commit, getters }, player) {
    if (getters.getPlayerById(player.user_id)) {
      commit(types.REMOVE_GAME_ROOM_PLAYER, player)
    }
  },

  setPlayers({ commit }, players) {
    commit(types.SET_GAME_ROOM_PLAYERS, players)
  },

  async getPlayers({ commit, getters }) {
    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `games/room/${code}/players`,
      method: 'get',
    })

    commit(types.SET_GAME_ROOM_PLAYERS, data)
  },

  async startNextRound({ commit }, { code, slug, round }) {
    const { data } = await fetchApi({
      input: `${slug}/${code}/startNextRound/${round}`,
      method: 'post',
    })

    commit(types.SET_GAME_ROOM, data)
    commit(types.SET_GAME_ROOM_PLAYERS, data.players)
  },

  async changeGameStage({ commit, getters }, stage) {
    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `games/room/${code}/stage`,
      method: 'put',
      data: { stage },
    })

    commit(types.SET_GAME_ROOM_DATA, data)
  },

  async exitGame({ getters, commit }) {
    const code = getters.getGameRoom.code

    await fetchApi({ input: `games/room/${code}/exit`, method: 'delete' })

    commit(types.RESET_GAME_ROOM)
  },

  async setPlayerOnTurn({ commit, getters }, userId) {
    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `games/room/${code}/playerOnTurn`,
      method: 'put',
      data: { userId },
    })

    commit(types.SET_GAME_ROOM_PLAYERS, data.players)
  },
}

const mutations = {
  [types.SET_GAMES](state, games) {
    state.games = games
  },

  [types.SET_GAME_ROOM](state, room) {
    state.playing.room = {
      code: room.code,
      rounds: room.rounds,
      gameId: room.game_id,
      startAt: room.start_at,
      createdBy: room.created_by,
      roundCounter: room.round_counter,
      data: room.data,
    }
  },

  [types.SET_GAME_ROOM_DATA](state, data) {
    state.playing.room.data = data
  },

  [types.RESET_GAME_ROOM](state) {
    state.playing.room = {}
    state.playing.players = []
  },

  [types.SET_GAME_ROOM_PLAYERS](state, players) {
    state.playing.players = players.map(x => ({
      id: x.user.id,
      name: x.user.name,
      avatar: x.user.avatar,
      level: x.user.level,
      status: x.status,
      points: x.points,
      data: x.data,
    }))

    state.playing.players = state.playing.players.sort((a, b) =>
      a.points < b.points ? 1 : b.points < a.points ? -1 : 0,
    )
  },

  [types.ADD_GAME_ROOM_PLAYER](state, player) {
    state.playing.players.push({
      id: player.user.id,
      name: player.user.name,
      avatar: player.user.avatar,
      level: player.user.level,
      status: player.status,
      points: player.points,
      data: player.data,
    })
  },

  [types.REMOVE_GAME_ROOM_PLAYER](state, player) {
    state.playing.players = state.playing.players.filter(
      x => x.id !== player.user_id,
    )
  },

  [types.SET_GAME_ROOM_FRIEND_INVITES](state, friendInvites) {
    state.invitedFriends = friendInvites.map(x => x.id)
  },

  [types.CHANGE_STATUS](state, player) {
    state.playing.players = state.playing.players.map(x => {
      if (x.id === parseInt(player.user_id, 10)) {
        x.status = player.status
      }
      return x
    })
  },
}

export default {
  state: gamesState,
  getters,
  actions,
  mutations,
}
