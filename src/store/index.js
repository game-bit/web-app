import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import user from './modules/user'
import friends from './modules/friends'
import loading from './modules/loading'
import games from './modules/games'
import dots from './modules/dots'
// import alert from './modules/alert'

import citiesCountries from './modules/cities-countries'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    user,
    friends,
    loading,
    games,
    dots,

    citiesCountries,
    // alert,
  },
  strict: process.env.NODE_ENV !== 'production',
})

export default store

const initialStateCopy = JSON.parse(JSON.stringify(store.state))

export function resetState() {
  store.replaceState(initialStateCopy)
}
