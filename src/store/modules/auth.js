import router from '@/router'
import { resetState } from '@/store'
import { fetchApi } from '@/utils'
import * as types from '../mutation-types'
import { Echo, authHeader } from '../../utils'

const authState = {
  accessToken: '',
}

const getters = {
  accessToken: function(state) {
    return state.accessToken
  },
}

const actions = {
  async signIn({ commit }, { data, provider, redirect }) {
    const { response, data: responseData } = await fetchApi({
      method: 'post',
      input: `auth/${provider}`,
      data,
    })

    if (response.ok) {
      commit(types.ADD_ACCESS_TOKEN, responseData.access_token)

      Echo.connector.pusher.config.auth.headers['Authorization'] = authHeader()

      if (redirect) return router.push(redirect)

      router.push({ name: 'games' })
    }
  },

  async signOut({ commit, dispatch }, tokenExpired = false) {
    if (tokenExpired) {
      // dispatch('showAlert', {
      //   type: 'error',
      //   message: 'Please sign in again to continue.',
      // })
    } else {
      await fetchApi({ method: 'post', input: 'sign-out' })
    }

    commit(types.REMOVE_ACCESS_TOKEN)

    dispatch('removeUser')

    resetState()

    router.push({ name: 'login' })
  },

  setAccessToken({ commit }, accessToken) {
    commit(types.ADD_ACCESS_TOKEN, accessToken)
  },
}

const mutations = {
  [types.ADD_ACCESS_TOKEN](state, accessToken) {
    state.accessToken = accessToken

    window.localStorage.setItem('accessToken', state.accessToken)
  },

  [types.REMOVE_ACCESS_TOKEN](state) {
    state.accessToken = ''

    window.localStorage.removeItem('accessToken')
  },
}

export default {
  state: authState,
  getters,
  actions,
  mutations,
}
