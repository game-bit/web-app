export default class Box {
  constructor(topLeft, topRight, bottomRight, bottomLeft, color, sketch) {
    this.topLeft = topLeft
    this.topRight = topRight
    this.bottomRight = bottomRight
    this.bottomLeft = bottomLeft
    this.color = color
    this.sketch = sketch
  }

  show() {
    let x1 = this.topLeft.x
    let y1 = this.topLeft.y
    let x2 = this.topRight.x
    let y2 = this.topRight.y
    let x3 = this.bottomRight.x
    let y3 = this.bottomRight.y
    let x4 = this.bottomLeft.x
    let y4 = this.bottomLeft.y

    this.sketch.fill(this.color)
    this.sketch.quad(x1, y1, x2, y2, x3, y3, x4, y4)
  }
}
