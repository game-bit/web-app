import store from '@/store'

export default async (to, _from, next) => {
  if (!store.getters.isGameRoomByCode(to.params.code)) {
    await store.dispatch('fetchGameRoom', to.params.code)
  }

  const room = store.getters.getGameRoom
  if (room && room.stage == 'getReady' && to.name !== 'game-lobby') {
    store.dispatch('startNextRound', {
      code: room.code,
      slug: to.name,
      round: room.roundCounter,
    })
  }

  if (store.getters.friends) {
    store.dispatch('fetchFriends')
  }

  next()
}
