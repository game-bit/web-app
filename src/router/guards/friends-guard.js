import store from '@/store'

export default async (to, from, next) => {
  store.dispatch('fetchFriends')
  store.dispatch('fetchFriendRequests')

  next()
}
