import { fetchApi } from '@/utils'
import * as types from '../mutation-types'

const setUserState = (state, data = {}) => {
  state.id = data.id || 0
  state.email = data.email || ''
  state.name = data.name || ''
  state.avatar = data.avatar || ''
}

const userState = {}

setUserState(userState)

const actions = {
  async fetchUser({ commit }) {
    const { data } = await fetchApi({ input: 'users/me' })

    commit(types.ADD_USER, data)
  },

  removeUser({ commit }) {
    commit(types.REMOVE_USER)
  },
}

const mutations = {
  [types.ADD_USER](state, data) {
    setUserState(state, data)
  },

  [types.REMOVE_USER](state) {
    setUserState(state)
  },
}

export default {
  state: userState,
  actions,
  mutations,
}
