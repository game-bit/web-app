export class ValidationError extends Error {
  constructor(errors = {}, ...params) {
    super(...params)

    Error.captureStackTrace(this, ValidationError)

    this.errors = errors
  }
}

export class AlertError extends Error {}
