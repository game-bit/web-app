import store from '@/store'
import { getAccessToken, Echo } from '@/utils'

export const authHeader = () => `Bearer ${getAccessToken()}`

export async function parseResponseData(response, handleAlerts) {
  const contentType = response.headers.get('content-type')
  const parsedResponse = { response }

  if (contentType.indexOf('application/json') !== -1) {
    parsedResponse.data = await response.json()

    if (!response.ok && !parsedResponse.data.type) {
      throw new Error(parsedResponse.data.message)
    }

    // if (!response.ok && parsedResponse.data.type === 'validation') {
    //   store.dispatch('showAlert', {
    //     type: 'error',
    //     message: i18n.t('validation.alertError'),
    //   })

    //   throw new ValidationError(parsedResponse.data.errors)
    // }

    // if (!response.ok && parsedResponse.data.type === 'alert') {
    //   store.dispatch('showAlert', {
    //     type: 'error',
    //     message: parsedResponse.data.message,
    //   })

    //   throw new AlertError(parsedResponse.data.message)
    // }

    responseAlertHandle(parsedResponse, handleAlerts)
  }

  if (contentType.indexOf('text/html') !== -1) {
    parsedResponse.data = await response.text()
  }

  return parsedResponse
}
export function responseAlertHandle(parsedResponse, handleAlerts = true) {
  if (!parsedResponse) return

  if (!handleAlerts) return

  // if (parsedResponse.data.type && parsedResponse.data.type === 'alert') {
  //   store.dispatch('showAlert', {
  //     type: parsedResponse.data.alert_type,
  //     message: parsedResponse.data.message,
  //   })
  // }
}

export const fetchApi = async ({
  method = 'get',
  input = '',
  data = {},
  init = {},
  handleAlerts = true,
}) => {
  if (method === 'put') {
    init.method = 'post'
    data._method = method
  } else {
    init.method = method
  }

  if (!init.headers) {
    init.headers = {}
  }

  init.headers.Accept = 'application/json'
  // init.headers['X-Locale'] = store.state.user.locale

  if (store.getters.accessToken !== '') {
    init.headers.Authorization = authHeader()
  }

  if (Echo.socketId()) {
    init.headers['X-Socket-ID'] = Echo.socketId()
  }

  if (method !== 'get' && Object.keys(data).length !== 0) {
    const body = new FormData()

    Object.keys(data).forEach(name => body.append(name, data[name]))

    init.body = body
  }

  const response = await fetch(`${process.env.VUE_APP_API_URL}/${input}`, init)

  if (response.status === 401 && input !== 'sign-out') {
    store.dispatch('signOut', true)
  }

  return parseResponseData(response, handleAlerts)
}
