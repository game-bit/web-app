import { fetchApi } from '@/utils'
import * as types from '../mutation-types'

const citiesCountriesState = {}

const getters = {
  getCountingPlayer: state =>
    state.playing ? state.playing.players.filter(x => x.data.isCounting) : {},
}

const actions = {
  async letterChosen({ getters }, letter) {
    let code = getters.getGameRoom.code

    await fetchApi({
      input: `cities-countries/${code}/letter`,
      method: 'post',
      data: {
        letter,
      },
    })
  },

  async endRound(
    { commit, getters, dispatch },
    { playerData, fireEvent, letter },
  ) {
    dispatch('startLoading', 'endRound')

    let code = getters.getGameRoom.code

    const { data } = await fetchApi({
      input: `cities-countries/${code}/end-round`,
      method: 'post',
      data: {
        playerData: JSON.stringify(playerData),
        fireEvent,
        letter,
      },
    })

    if (data) {
      if (data.data.stage !== 'rating') {
        dispatch('changeGameStage', 'rating')
      }
      commit(types.SET_GAME_ROOM, data)
      commit(types.SET_GAME_ROOM_PLAYERS, data.players)

      dispatch('stopLoading', 'endRound')
    }
  },

  setEndRound({ commit, dispatch }, room) {
    commit(types.SET_GAME_ROOM, room)
    commit(types.SET_GAME_ROOM_PLAYERS, room.players)

    dispatch('stopLoading', 'endRound')
  },
}

const mutations = {}

export default {
  state: citiesCountriesState,
  getters,
  actions,
  mutations,
}
