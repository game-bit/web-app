// eslint-disable-next-line
import Pusher from 'pusher-js'
import Echo from 'laravel-echo'

export default new Echo({
  broadcaster: 'pusher',
  key: process.env.VUE_APP_PUSHER_KEY,
  cluster: 'eu',
  encrypted: true,
  authEndpoint: `${process.env.VUE_APP_API_URL}/broadcasting/auth`,
})
