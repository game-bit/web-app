import { fetchApi } from '@/utils'
import * as types from '../mutation-types'

const dotsState = {
  boxes: [],
  lines: [],
  dots: [],
}

const getters = {
  getBoxes: state => state.boxes,
  getLines: state => state.lines,
  getDots: state => state.dots,
}

const actions = {
  setDotsState({ commit, getters }) {
    let room = getters.getGameRoom

    if (room.data.dots.length > 0) {
      commit(types.SET_DOTS, room.data.dots)
    }
    commit(types.SET_LINES, room.data.lines)
    commit(types.SET_BOXES, room.data.boxes)
  },

  addDots({ commit, getters }, dots) {
    let code = getters.getGameRoom.code
    commit(types.ADD_DOTS, dots)

    fetchApi({
      input: `dots/${code}/dots`,
      method: 'post',
      data: { dots: JSON.stringify(dots) },
    })
  },

  setDotDir({ commit }, { dot, dir }) {
    commit(types.SET_DOT_DIR, { dot, dir })
  },

  async addLine({ commit, getters }, { line, dot, dir }) {
    let code = getters.getGameRoom.code
    commit(types.ADD_LINE, line)

    const { data } = await fetchApi({
      input: `dots/${code}/line`,
      method: 'post',
      data: { line: JSON.stringify(line), dot: JSON.stringify(dot), dir },
    })

    commit(types.SET_GAME_ROOM, data)
    commit(types.SET_GAME_ROOM_PLAYERS, data.players)
  },

  async addBox({ commit, getters }, box) {
    let code = getters.getGameRoom.code

    commit(types.ADD_BOX, box)

    const { data } = await fetchApi({
      input: `dots/${code}/box`,
      method: 'post',
      data: { box: JSON.stringify(box) },
    })

    commit(types.SET_GAME_ROOM, data)
    commit(types.SET_GAME_ROOM_PLAYERS, data.players)
  },
}

const mutations = {
  [types.ADD_DOTS](state, dots) {
    state.dots = dots
  },

  [types.ADD_BOX](state, box) {
    state.boxes.push(box)
  },

  [types.ADD_LINE](state, line) {
    state.lines.push(line)
  },

  // eslint-disable-next-line prettier/prettier
  [types.SET_DOT_DIR](state, { dot: { i, j }, dir }) {
    state.dots[i][j][dir] = true
  },

  [types.SET_DOTS](state, dots) {
    state.dots = dots
  },

  [types.SET_BOXES](state, boxes) {
    state.boxes = boxes
  },

  [types.SET_LINES](state, lines) {
    state.lines = lines
  },
}

export default {
  state: dotsState,
  getters,
  actions,
  mutations,
}
