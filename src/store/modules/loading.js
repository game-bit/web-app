import * as types from '../mutation-types'

const loadingState = {
  activeLoaders: [],
  loaded: [],
}

const getters = {
  isLoading: state => name => state.activeLoaders.includes(name),

  isLoaded: state => name => state.loaded.includes(name),
}

const actions = {
  startLoading: ({ commit }, name) => commit(types.START_ACTIVE_LOADING, name),

  stopLoading: ({ commit }, name) => commit(types.STOP_ACTIVE_LOADING, name),
}

const mutations = {
  [types.START_ACTIVE_LOADING]: (state, name) => state.activeLoaders.push(name),

  [types.STOP_ACTIVE_LOADING]: (state, name) => {
    state.activeLoaders = state.activeLoaders.filter(x => x !== name)
  },
}

export default {
  state: loadingState,
  getters,
  actions,
  mutations,
}
