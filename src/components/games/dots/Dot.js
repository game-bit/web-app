export default class Dot {
  constructor(x, y, size, sketch) {
    this.x = x
    this.y = y
    this.right = false
    this.down = false
    this.size = size
    this.sketch = sketch
  }

  show() {
    this.sketch.noStroke()
    this.sketch.fill(0)

    this.sketch.circle(this.x, this.y, this.size)
  }
}
