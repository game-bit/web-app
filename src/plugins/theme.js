export default {
  primary: {
    base: '#E4002B',
    lighten1: '#F17F95',
    lighten2: '#FDEBEE',
  },
  accent: '#E4002B',
  error: '#E4002B',
  info: '#4B67D6',
  success: {
    base: '#0CCE6B',
    lighten1: '#85E6B4',
  },
  warning: '#FFDF00',
  black: '#212121',
  grey: {
    base: '#8D99AE',
    darken1: '#727272',
    lighten1: '#C1CAD6',
    lighten2: '#E0E4EA',
    lighten3: '#EFF2F5',
    lighten4: '#FAFBFC',
  },
  first: '#FFDF00',
  second: '#C1CAD6',
  third: '#EAC435',
  facebook: '#3B5998',
}
