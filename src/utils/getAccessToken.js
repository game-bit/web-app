import store from '@/store'

export default function() {
  if (store.getters.accessToken === '') {
    const accessToken = window.localStorage.getItem('accessToken')

    if (accessToken !== null) {
      store.dispatch('setAccessToken', accessToken)
    }
  }

  return store.getters.accessToken
}
