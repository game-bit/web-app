import { fetchApi } from '@/utils'
import * as types from '../mutation-types'

const authState = {
  friends: [],
  requests: [],
  sendRequests: [],
  search: [],
}

const getters = {
  friends: state => state.friends,
  friendRequests: state => state.requests,
  search: state => state.search,
  getSendRequestById: state => friendId =>
    state.sendRequests.find(x => x && x.id === friendId),
  getSearchFriendById: state => friendId =>
    state.search.find(x => x.id === friendId),
}

const actions = {
  async fetchFriends({ commit, dispatch }) {
    dispatch('startLoading', 'fetchFriends')

    const { data } = await fetchApi({ input: 'friends' })

    commit(types.SET_FRIENDS, data)

    dispatch('stopLoading', 'fetchFriends')
  },

  async search({ commit }, query) {
    const { data } = await fetchApi({
      input: 'friends/search',
      method: 'post',
      data: { query },
    })

    commit(types.SET_SEARCH, data)
  },

  async fetchFriendRequests({ commit, dispatch }) {
    dispatch('startLoading', 'fetchFriendRequests')
    const { data } = await fetchApi({ input: 'friends/request' })

    commit(types.SET_FRIEND_REQUESTS, data)
    dispatch('stopLoading', 'fetchFriendRequests')
  },

  async fetchSendFriendRequests({ commit }) {
    const { data } = await fetchApi({ input: 'friends/request/send' })

    commit(types.SET_SEND_FRIEND_REQUESTS, data)
  },

  async changeStatus({ commit, dispatch }, { status, friendId }) {
    if (status === 'accepted')
      dispatch('startLoading', `changeStatusAccepted${friendId}`)
    else if (status === 'rejected')
      dispatch('startLoading', `changeStatusRejected${friendId}`)

    await fetchApi({
      input: `friends/${friendId}`,
      method: 'put',
      data: { status },
    })

    await dispatch('fetchFriends')

    commit(types.REMOVE_FRIEND_REQUESTS, friendId)

    if (status === 'accepted')
      dispatch('stopLoading', `changeStatusAccepted${friendId}`)
    else if (status === 'rejected')
      dispatch('stopLoading', `changeStatusRejected${friendId}`)
  },

  async addFriend({ commit, dispatch, getters }, friendId) {
    if (!getters.getSendRequestById(friendId)) {
      dispatch('startLoading', `addFriend${friendId}`)

      await fetchApi({
        input: `friends`,
        method: 'post',
        data: { friendId },
      })

      commit(
        types.ADD_SEND_FRIEND_REQUEST,
        getters.getSearchFriendById(friendId),
      )

      dispatch('stopLoading', `addFriend${friendId}`)
    }
  },

  addFriendRequest({ commit, getters }, friend) {
    if (!getters.friendRequests.find(x => x.id === friend.id)) {
      commit(types.ADD_FRIEND_REQUESTS, friend)
    }
  },
}

const mutations = {
  [types.SET_FRIENDS](state, friends) {
    state.friends = friends
  },

  [types.SET_FRIEND_REQUESTS](state, friendRequests) {
    state.requests = friendRequests
  },

  [types.ADD_FRIEND_REQUESTS]: (state, friend) => state.requests.push(friend),

  [types.SET_SEND_FRIEND_REQUESTS](state, friendRequests) {
    state.sendRequests = friendRequests
  },

  [types.SET_SEARCH](state, search) {
    state.search = search
  },

  [types.REMOVE_FRIEND_REQUESTS](state, friendId) {
    state.requests = state.requests.filter(x => x.id !== friendId)
  },

  [types.ADD_SEND_FRIEND_REQUEST]: (state, friend) =>
    state.sendRequests.push(friend),
}

export default {
  state: authState,
  getters,
  actions,
  mutations,
}
