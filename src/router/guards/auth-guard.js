import store from '@/store'
import { getAccessToken } from '@/utils'

export default async (to, from, next) => {
  if (getAccessToken() === '') {
    return next({ name: 'login', query: { next: to.fullPath } })
  }

  await store.dispatch('fetchUser')

  next()
}
