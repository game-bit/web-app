export default class Line {
  constructor(x, y, w, h, color, sketch) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
    this.color = color
    this.sketch = sketch
  }

  show() {
    this.sketch.fill(this.color)
    this.sketch.noStroke()

    this.sketch.rect(this.x, this.y, this.w, this.h)
  }
}
