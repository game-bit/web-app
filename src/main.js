import Vue from 'vue'
import VueEcho from 'vue-echo'
import './plugins/vuetify'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import './registerServiceWorker'
import { Echo, authHeader, getAccessToken } from './utils'

import RouteWrapper from './components/RouteWrapper'
import GameWrapper from './components/GameWrapper'
import Loading from './components/Loading'

import Logo from './components/icons/Logo'

Vue.config.productionTip = false

if (getAccessToken()) {
  Echo.connector.pusher.config.auth.headers['Authorization'] = authHeader()
}

Vue.use(VueEcho, Echo)

window.fbAsyncInit = function() {
  window.FB.init({
    appId: process.env.VUE_APP_FACEBOOK_APP_ID,
    cookie: true,
    xfbml: true,
    version: process.env.VUE_APP_FACEBOOK_API_VERSION,
  })

  window.FB.AppEvents.logPageView()
}
;(function(d, s, id) {
  const fjs = d.getElementsByTagName(s)[0]
  if (d.getElementById(id)) {
    return
  }
  const js = d.createElement(s)
  js.id = id
  js.src = 'https://connect.facebook.net/en_US/sdk.js'
  fjs.parentNode.insertBefore(js, fjs)
})(document, 'script', 'facebook-jssdk')

// Global components
Vue.component('route-wrapper', RouteWrapper)
Vue.component('game-wrapper', GameWrapper)
Vue.component('loading', Loading)
Vue.component('logo', Logo)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
