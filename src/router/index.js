import Vue from 'vue'
import Router from 'vue-router'

import AuthGuard from './guards/auth-guard'
import FriendsGuard from './guards/friends-guard'
import AddFriendGuard from './guards/add-friend-guard'
import GamesGuard from './guards/games-guard'
import PlayingGameGuard from './guards/playing-game-guard'

import Auth from '@/views/Auth'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import Games from '@/views/Games'
import Game from '@/views/Game'
import Friends from '@/views/Friends'
import AddFriend from '@/views/AddFriend'
import GameLobby from '@/views/GameLobby'
import CitiesCountries from '@/views/CitiesCountries'
import Dots from '@/views/Dots'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/auth',
      component: Auth,
      children: [
        {
          path: 'login',
          name: 'login',
          component: Login,
        },
        {
          path: 'sign-up',
          name: 'signUp',
          component: SignUp,
        },
      ],
    },
    {
      path: '',
      component: {
        render: c =>
          c('router-view', {
            mounted() {
              console.log(this.$echo)
            },
          }),
      },
      beforeEnter: AuthGuard,
      redirect: 'games',
      children: [
        {
          path: 'games',
          component: {
            render: c =>
              c('router-view', {
                mounted() {
                  console.log(this.$echo)
                },
              }),
          },
          beforeEnter: GamesGuard,
          children: [
            {
              path: '/',
              component: Games,
              name: 'games',
            },
            {
              name: 'game',
              path: ':gameId',
              component: Game,
            },
            {
              name: 'game-lobby',
              path: 'lobby/:code',
              component: GameLobby,
              beforeEnter: PlayingGameGuard,
            },
            {
              name: 'cities-countries',
              path: 'cities-countries/:code',
              component: CitiesCountries,
              beforeEnter: PlayingGameGuard,
            },
            {
              name: 'dots',
              path: 'dots/:code',
              component: Dots,
              beforeEnter: PlayingGameGuard,
            },
          ],
        },
        {
          path: 'friends',
          component: { render: c => c('router-view') },
          children: [
            {
              path: '/',
              name: 'friends',
              component: Friends,
              beforeEnter: FriendsGuard,
            },
            {
              path: 'add',
              component: AddFriend,
              name: 'add-friend',
              beforeEnter: AddFriendGuard,
            },
          ],
        },
      ],
    },
  ],
})
